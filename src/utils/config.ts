import config from 'config';

export const apiConfig: Config['api'] = config.get('api');
export const connectionConfig: Config['connection'] = config.get('connection');
export const securityConfig: Config['security'] = config.get('security');
export const clientConfig: Config['client'] = config.get('client');
export const smtpConfig: Config['smtp'] = config.get('smtp');
