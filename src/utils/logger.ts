/**
 * Logger utils for pretty print in console
 */
export class Logger {
    /**
     * Log info in cyan color with formatted time and message
     */
    static info(message: any) {
        console.log(`[\x1b[36mINFO\x1b[0m] ${this._formatTime()} ${message}`);
    }
    /**
     * Log error in red color with formatted time and message
     */
    static error(message: any) {
        console.log(`[\x1b[31mERROR\x1b[0m] ${this._formatTime()} ${message}`);
    }
    /**
     * Format time for log
     */
    private static _formatTime(): string {
        const date = new Date();
        const hours = date.getHours() >= 10 ? date.getHours() : '0' + date.getHours();
        const minutes = date.getMinutes() >= 10 ? date.getMinutes() : '0' + date.getMinutes();
        const seconds = date.getSeconds() >= 10 ? date.getSeconds() : '0' + date.getSeconds();
        return `\x1b[90m${hours}:${minutes}:${seconds}\x1b[0m`;
    }
}
