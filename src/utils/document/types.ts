export interface IDocument {
    title: string;
    chapters: IChapter[];
    paragraphs?: IParagraph[];
    version?: number;
    revision?: number;
    versionInfos?: any[];
}

export interface IParagraph {
    content: string;
    images: { src: string; name?: string }[];
}

export interface IChapter {
    title: string;
    paragraphs: IParagraph[];
    subChapters: IChapter[];
    image?: string;
}

export interface IVersionInfo {
    author: string;
    action: string;
    date: string;
}

export enum DocumentReaderEventType {
    LOADED = 'LOADED',
}
