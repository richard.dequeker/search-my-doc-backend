import { EventEmitter } from 'events';
import { readFileSync } from 'fs';

import { DocumentReaderEventType } from './types';

export abstract class DocumentReader extends EventEmitter {
    readonly EventType = DocumentReaderEventType;

    protected readonly documentBuffer: Buffer;

    constructor({ path, buffer }: { path?: string; buffer?: Buffer }) {
        super();
        if (path) {
            this.documentBuffer = readFileSync(path);
        } else if (buffer) {
            this.documentBuffer = buffer;
        } else {
            throw new Error('Need a path or a buffer');
        }
        this.load();
    }
    // TODO Impl
    abstract getTitle(): string;

    abstract getVersion(): number;

    abstract getRevision(): number;

    abstract getVersionInfos(): any[];

    abstract getChapters(): any[];

    protected abstract load(): Promise<void>;
}
