import { IDocument } from './types';
import { DocumentReader } from './document-reader';

export abstract class DocumentBuilder<T extends IDocument> {
    protected abstract document: T;

    constructor(private reader: DocumentReader) {}

    buildDocument(): Promise<void> {
        return new Promise(resolve => {
            this.reader.on(this.reader.EventType.LOADED, () => {
                this.document.title = this.reader.getTitle();
                this.document.version = this.reader.getVersion();
                this.document.revision = this.reader.getRevision();
                this.document.versionInfos = this.reader.getVersionInfos();
                this.document.chapters = this.reader.getChapters();
                resolve();
            });
        });
    }

    getDocument(): T {
        return this.document;
    }
}
