import casual from 'casual';

export interface CasualOptions {
    [key: string]: keyof Casual.Casual;
}

export class Random {
    /**
     * Create one datum with random fields
     * @example
     * const user: User = createRandom<User>({ id: 'uuid', email: 'email' });
     */
    static createOne<T>(options: CasualOptions): T {
        const datum: any = {};
        Object.keys(options).forEach((key: string) => (datum[key] = casual[options[key]]));
        return datum as T;
    }
    /**
     * Create array of data with random fields
     * @example
     * const users: User[] = createMany<User>(10, { id: 'uuid', email: 'email' });
     */
    static createManyRandom<T>(count: number, options: CasualOptions): T[] {
        const data: any[] = [];
        for (let i = 0; i < count; i++) {
            data.push(this.createOne(options));
        }
        return data as T[];
    }
    /**
     * Merge one or more fields with random value to datum
     * @example
     * const user: Partial<User> = createRandom<User>({ email: 'email' });
     * const userWithId: User = addFieldsWithRandomValue(user, { id: 'uuid' });
     */
    static addFields<T>(datum: T, options: CasualOptions): T {
        return { ...datum, ...this.createOne<T>(options) } as T;
    }
    /**
     * Merge one or more fiels with random value to array of data
     * @example
     * const users: Partial<User>[] = createManyRandom<Partial<User>>({ email: 'email' });
     * const usersWithId: User[] = addFieldsToMany<User>({ id: 'uuid' });
     */
    static addFieldsToMany<T>(data: T[], options: CasualOptions): T[] {
        return data.map(datum => this.addFields(datum, options)) as T[];
    }

    /**
     * return a random integer between 0 and max value
     * @example
     * const randomNumber = number(2) // 0, 1 or 2
     */
    static number(min: number = 1, max?: number) {
        const random = Math.floor(Math.random() * (max ? max : min + 1));
        return max ? random + min : random;
    }

    /**
     * return randomly true or false
     */
    static bool() {
        return this.number() ? true : false;
    }

    /**
     * return a copy of array with random length and values
     */
    static from<T>(arr: T[], max?: number): T[] {
        const filtered = arr.filter(() => {
            const pass = this.bool();
            return pass;
        });
        return filtered.slice(0, max && max < arr.length ? max : arr.length - 1);
    }

    /**
     * return one random value of array
     */
    static oneFrom<T>(data: T[]): T {
        if (!data.length) {
            throw new Error('Data array must have a length');
        }
        return data[this.number(data.length - 1)];
    }

    /**
     * return casual lib
     */
    static casual(key: keyof Casual.Casual) {
        return casual[key];
    }
}
