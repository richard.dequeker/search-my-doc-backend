export type FunctionPropertyNames<T> = NonNullable<
    { [K in keyof T]: T[K] extends () => void ? K : never }[keyof T]
>;

export type FunctionProperties<T> = Pick<T, FunctionPropertyNames<T>>;

export type NonFunctionPropertyNames<T> = NonNullable<
    { [K in keyof T]: T[K] extends () => void ? never : K }[keyof T]
>;

export type NonFunctionProperties<T> = Pick<T, NonFunctionPropertyNames<T>>;

export type ObjectPropertyNames<T> = NonNullable<
    { [K in keyof T]: T[K] extends object ? K : never }[keyof T]
>;

export type ObjectProperties<T> = Pick<T, ObjectPropertyNames<T>>;

export type ObjectWithoutFunction<T> = Omit<T, FunctionPropertyNames<T>>;
