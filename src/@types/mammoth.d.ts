declare module 'mammoth' {
    export interface MammothInput {
        path?: string;
        buffer?: Buffer;
    }
    export interface MammothOptions {
        [key: string]: any;
    }

    export interface MammothResult {
        value: string;
        messages: any[];
    }

    export function convertToHtml(
        input: MammothInput,
        options?: MammothOptions
    ): Promise<MammothResult>;
}
