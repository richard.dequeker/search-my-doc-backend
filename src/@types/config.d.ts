declare interface Config {
    connection: import('typeorm').ConnectionOptions;
    api: {
        url: string;
        port: number;
        paths: {
            resetPassword: string;
        };
    };
    client: {
        url: string;
        redirections: {
            resetPassword: string;
        };
    };
    security: {
        cors: {
            origin: string;
        };
        jwt: {
            secret: string;
        };
        administration: {
            email: string;
            password: string;
            domain: {
                name: string;
                description: string;
            };
        };
    };
    smtp: {
        host: string;
        port: number;
        email: string;
        password: string;
    };
}
