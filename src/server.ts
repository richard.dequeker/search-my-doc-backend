import { ContextFunction } from 'apollo-server-core';
import { ApolloServer, ApolloServerExpressConfig } from 'apollo-server-express';
import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';
import { ServerInfo } from 'apollo-server';
import express, { Application, Router, Request, Response, NextFunction } from 'express';
import { GraphQLError } from 'graphql';
import { buildSchema, AuthChecker, MiddlewareFn } from 'type-graphql';
import { Container } from 'typedi';
import { ConnectionOptions, useContainer, createConnections, createConnection } from 'typeorm';

export interface Controller {
    path: string;
    router: Router;
}

export interface ServerOptions {
    port?: string | number;
    cors: { origin: string };
    resolvers: any[]; // graphQL resolvers
    express?: ApolloServerExpressConfig;
    connection?: ConnectionOptions;
    context?: object | ContextFunction<ExpressContext, object>;
    authChecker?: AuthChecker<any, any>;
    formatError?: (error: GraphQLError) => any;
    globalMiddlewares?: MiddlewareFn[];
    controllers?: Controller[];
}

export class Server {
    port: string | number;
    cors: { origin: string };
    connection?: ConnectionOptions;
    resolvers: any[];
    controllers: any[];
    context?: object | ContextFunction<ExpressContext, object>;
    authChecker?: AuthChecker<any, any>;
    formatError?: (error: GraphQLError) => any;
    globalMiddlewares?: MiddlewareFn[];

    app: Application;
    schema: any;

    private server: ApolloServer | null = null;

    constructor({
        port = 4000,
        cors = { origin: '*' },
        connection,
        resolvers = [],
        controllers = [],
        context,
        authChecker,
        formatError,
    }: ServerOptions) {
        this.port = port;
        this.cors = cors;
        this.connection = connection;
        this.resolvers = resolvers;
        this.controllers = controllers;
        this.context = context;
        this.authChecker = authChecker;
        this.formatError = formatError;
        this.app = express();
    }
    /**
     * Start server
     */
    async start(): Promise<Partial<ServerInfo>> {
        try {
            await this.createConnection();
            await this.createSchema();
            await this.setControllers();
            return await this.createServerAndListen();
        } catch (err) {
            throw err;
        }
    }
    /**
     * Stop server
     */
    stop() {
        if (this.server) {
            this.server.stop();
        }
    }
    /**
     * Create database connection
     */
    private async createConnection() {
        if (this.connection) {
            useContainer(Container);
            await createConnection(this.connection);
        }
    }
    /**
     * generate GraphQL schema
     */
    private async createSchema(): Promise<void> {
        this.schema = await buildSchema({
            resolvers: this.resolvers ? this.resolvers : [],
            container: Container,
            authChecker: this.authChecker,
            globalMiddlewares: this.globalMiddlewares,
        });
    }
    /**
     * Rest controllers
     */
    private setControllers() {
        for (const controller of this.controllers) {
            this.app.use(controller.path, controller.router);
        }
        this.app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            if (err.code) {
                switch (err.code) {
                    case 'NOT_FOUND':
                        res.status(404);
                        break;
                    default:
                        res.status(500);
                }
            } else {
                res.status(500);
            }
            return res.json({ message: err.message });
        });
    }
    /**
     * Create server instance and start
     */
    private createServerAndListen(): Promise<Partial<ServerInfo>> {
        this.server = new ApolloServer({
            schema: this.schema,
            formatError: this.formatError,
            context: this.context,
            introspection: process.env.NODE_ENV === 'development',
        });

        this.server.applyMiddleware({ app: this.app, cors: this.cors });

        return new Promise((resolve, reject) => {
            this.app.listen({ port: this.port }, () =>
                resolve({ url: 'http://localhost:' + this.port })
            );
        });
    }
}
