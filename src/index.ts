import 'reflect-metadata';

import { Logger } from './utils/logger';
import { Server, Controller } from './server';
import { User, Domain, Document, DocumentArchive, Rule, RuleArchive, EmailUUID } from './core';
import {
    appContext,
    authChecker,
    UserResolver,
    DocumentResolver,
    DomainResolver,
    RuleResolver,
    AuthResolver,
    formatError,
    LogRequest,
    SeederResolver,
} from './api';
import Container from 'typedi';
import { SeederService } from './api/services';
import { MailController } from './api/controllers';
import { apiConfig, connectionConfig, securityConfig } from './utils/config';

const IS_DEV = process.env.NODE_ENV === 'development';

// TypeORM entities
const entities = [Document, DocumentArchive, Rule, RuleArchive, Domain, User, EmailUUID];
// GraphQL resolvers
const resolvers: any[] = [
    UserResolver,
    AuthResolver,
    DocumentResolver,
    DomainResolver,
    RuleResolver,
];

if (IS_DEV) {
    resolvers.push(SeederResolver);
}

// Rest controllers
const controllers: Controller[] = [
    {
        path: apiConfig.paths.resetPassword,
        router: MailController,
    },
];

/**
 * MAIN
 */
(async () => {
    const server = new Server({
        connection: { ...connectionConfig, entities },
        cors: securityConfig.cors,
        port: apiConfig.port,
        context: appContext,
        authChecker,
        resolvers,
        globalMiddlewares: [LogRequest],
        formatError,
        controllers,
    });
    try {
        const { url } = await server.start();
        await seedAdmin();
        Logger.info(`Server ready at ${url}`);
    } catch (error) {
        Logger.error(error);
        if (IS_DEV) {
            console.error(error);
        }
    }
})();

async function seedAdmin() {
    const seederService = Container.get(SeederService);
    try {
        const admin = await seederService.seedAdmin(
            securityConfig.administration.domain,
            securityConfig.administration.email,
            securityConfig.administration.password
        );
        return admin;
    } catch (error) {
        Logger.error(error);
    }
}
