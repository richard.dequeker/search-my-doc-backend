import { Field, ObjectType, Int } from 'type-graphql';

@ObjectType()
export class PartialRuleType {
    @Field(type => Int)
    version!: number;

    @Field(type => Int)
    revision!: number;

    @Field()
    title!: string;

    @Field()
    content!: string;

    @Field({ nullable: true })
    image?: string;
}
