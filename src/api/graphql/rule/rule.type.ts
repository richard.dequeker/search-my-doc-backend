import { Field, ObjectType, Int } from 'type-graphql';

import { NodeType } from '../common';
import { DomainType } from '../domain';

import '../common/archive-status.enum';

@ObjectType()
export class RuleType extends NodeType {
    @Field(type => Int)
    version!: number;

    @Field(type => Int)
    revision!: number;

    @Field()
    title!: string;

    @Field()
    content!: string;

    @Field({ nullable: true })
    image?: string;

    @Field(type => DomainType)
    domain!: DomainType;
}
