import { Field, ObjectType } from 'type-graphql';

import { ArchiveStatus } from 'src/core';
import { RuleType } from './rule.type';
import '../common/archive-status.enum';

@ObjectType()
export class RuleArchiveType extends RuleType {
    @Field(type => ArchiveStatus)
    status!: ArchiveStatus;

    @Field(type => RuleType)
    draft!: RuleType;
}
