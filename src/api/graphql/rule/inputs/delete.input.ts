import { Field, ID, InputType } from 'type-graphql';

@InputType()
export class DeleteRuleInput {
    @Field(type => ID)
    id!: string;
}
