import { Field, InputType, ID } from 'type-graphql';

@InputType()
export class CreateRuleInput {
    @Field()
    title!: string;

    @Field()
    content!: string;

    @Field()
    version!: number;

    @Field()
    revision!: number;

    @Field(type => ID)
    domainId!: string;

    @Field({ nullable: true })
    image?: string;
}
