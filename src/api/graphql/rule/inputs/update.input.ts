import { Field, ID, InputType } from 'type-graphql';

@InputType()
export class UpdateRuleInput {
    @Field(type => ID)
    id!: string;

    @Field({ nullable: true })
    title?: string;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;

    @Field({ nullable: true })
    image?: string;

    @Field({ nullable: true, defaultValue: false })
    major?: boolean;
}
