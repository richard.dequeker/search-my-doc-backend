import { Field, ID, InputType } from 'type-graphql';

@InputType()
export class PublishRuleInput {
    @Field(type => ID)
    id!: string;
}
