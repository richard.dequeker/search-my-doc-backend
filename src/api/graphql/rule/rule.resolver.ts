import { Resolver, Query, Mutation, Arg, Args } from 'type-graphql';

import { Rule } from 'src/core';
import { RuleType } from './rule.type';
import { RuleService } from 'src/api/services';
import {
    FindRulesArgs,
    FindOneRuleArgs,
    FindRuleArchivesArgs,
    FindOneRuleArchiveArgs,
} from './args';
import { CreateRuleInput, UpdateRuleInput, DeleteRuleInput } from './inputs';
import { RuleArchiveType } from './rule-archive.type';
import { PublishRuleInput } from './inputs/publish.input';

@Resolver(of => RuleType)
export class RuleResolver {
    constructor(private readonly ruleService: RuleService) {}
    @Query(returns => [RuleType], { description: 'Find many rules' })
    async rules(@Args() where?: FindRulesArgs): Promise<Rule[]> {
        if (where?.domainId) {
            const domainId = where.domainId;
            delete where.domainId;
            return this.ruleService.find({ where: { ...where, domain: { id: domainId } } });
        }
        return await this.ruleService.find({ where });
    }
    @Query(returns => RuleType, { description: 'Find one rule' })
    async rule(@Args() where?: FindOneRuleArgs): Promise<Rule> {
        if (where?.domainId) {
            const domainId = where.domainId;
            delete where.domainId;
            return this.ruleService.findOne({ where: { ...where, domain: { id: domainId } } });
        }
        return await this.ruleService.findOne({ where });
    }
    @Query(returns => [RuleArchiveType], { description: 'Find many rules' })
    async ruleArchives(@Args() where?: FindRuleArchivesArgs): Promise<Rule[]> {
        let finalWhere: any = null;
        if (where?.domainId) {
            const domainId = where.domainId;
            finalWhere = { domain: { id: domainId } };
            delete where.domainId;
        }
        if (where?.draftId) {
            const draftId = where.draftId;
            finalWhere = { draft: { id: draftId }, ...finalWhere };
            delete where.draftId;
        }
        return await this.ruleService.findArchive({ where: { ...where, ...finalWhere } });
    }
    @Query(returns => RuleArchiveType, { description: 'Find one rule archive' })
    async ruleArchive(@Args() where?: FindOneRuleArchiveArgs): Promise<Rule> {
        let finalWhere: any = null;
        if (where?.domainId) {
            const domainId = where.domainId;
            finalWhere = { domain: { id: domainId } };
            delete where.domainId;
        }
        if (where?.draftId) {
            const draftId = where.draftId;
            finalWhere = { draft: { id: draftId }, ...finalWhere };
            delete where.draftId;
        }
        return await this.ruleService.findOneArchive({ where: { ...where, ...finalWhere } });
    }
    @Mutation(returns => RuleArchiveType, { description: 'Publish one rule' })
    async publishRule(@Arg('rule') rule: PublishRuleInput): Promise<Rule> {
        return await this.ruleService.publish(rule);
    }
    @Mutation(returns => RuleArchiveType, { description: 'Create one rule' })
    async createRule(@Arg('rule') rule: CreateRuleInput): Promise<Rule> {
        return await this.ruleService.create(rule);
    }
    @Mutation(returns => RuleType, { description: 'Update one rule' })
    async updateRule(@Arg('rule') rule: UpdateRuleInput) {
        return await this.ruleService.update(rule);
    }
    @Mutation(returns => String, { description: 'Delete one rule' })
    async deleteRule(@Arg('rule') rule: DeleteRuleInput) {
        return await this.ruleService.delete(rule.id);
    }
}
