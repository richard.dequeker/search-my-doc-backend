import { Field, ArgsType, ID } from 'type-graphql';

import { RuleType } from '../rule.type';

@ArgsType()
export class FindRulesArgs {
    @Field({ nullable: true })
    id?: string;

    @Field({ nullable: true })
    title?: string;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;
}
