import { ArgsType, Field, ID } from 'type-graphql';

@ArgsType()
export class FindOneRuleArgs {
    @Field(type => ID, { nullable: true })
    id?: string;

    @Field({ nullable: true })
    title?: string;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;
}
