import { Field, ArgsType, ID } from 'type-graphql';
import { ArchiveStatus } from 'src/core';

@ArgsType()
export class FindRuleArchivesArgs {
    @Field({ nullable: true })
    id?: string;

    @Field({ nullable: true })
    title?: string;

    @Field(type => ArchiveStatus, { nullable: true })
    status?: ArchiveStatus;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;

    @Field(type => ID, { nullable: true })
    draftId?: string;
}
