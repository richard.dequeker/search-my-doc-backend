import { ObjectType, Field } from 'type-graphql';

import { UserType } from '../user';

@ObjectType()
export class AuthType {
    @Field(type => UserType)
    user!: UserType;

    @Field()
    token!: string;
}
