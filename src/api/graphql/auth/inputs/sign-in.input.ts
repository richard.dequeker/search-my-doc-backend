import { InputType, Field } from 'type-graphql';
import { IsEmail, MinLength } from 'class-validator';

@InputType()
export class SignInInput {
    @Field()
    @IsEmail()
    email!: string;

    @Field()
    @MinLength(6)
    password!: string;
}
