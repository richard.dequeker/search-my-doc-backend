import { Resolver, Mutation, Arg, Ctx, Query } from 'type-graphql';

import { AuthService, UserService } from 'src/api/services';
import { AuthType } from './auth.type';
import { SignInInput } from './inputs';

import { AuthContext } from 'src/api/context/types';
import { MailService } from 'src/api/services/mail.service';
@Resolver(of => AuthType)
export class AuthResolver {
    constructor(
        private authService: AuthService,
        private userService: UserService,
        private emailService: MailService
    ) {}

    @Mutation(returns => AuthType, { description: 'Sign in with email and password' })
    async signIn(@Arg('credentials') { email, password }: SignInInput): Promise<AuthType> {
        return await this.authService.signIn(email, password);
    }

    @Query(returns => AuthType, { description: 'Authenticate with header' })
    async authenticate(@Ctx() ctx: AuthContext): Promise<AuthType> {
        return await this.authService.authenticate(ctx);
    }

    @Query(returns => String, { description: 'Send mail for password reset' })
    async resetPassword(@Arg('email') email: string): Promise<string> {
        const user = await this.userService.findOne({ where: { email } });
        await this.emailService.sendMail(user);
        return 'Mail sended';
    }
}
