import { Resolver, Query, Arg } from 'type-graphql';

import { SeederService } from '../services';
import { UserType } from './user';
import { DomainType } from './domain';

@Resolver()
export class SeederResolver {
    constructor(private readonly seederService: SeederService) {}

    @Query(returns => [DomainType])
    async createRandomDomains(@Arg('count', { nullable: true }) count?: number) {
        return await this.seederService.seedRandomDomains(count);
    }

    @Query(returns => [UserType])
    async createRandomUsers(
        @Arg('count', { nullable: true }) count?: number,
        @Arg('password', { nullable: true }) password?: string
    ) {
        return await this.seederService.seedRandomUsers(count, password);
    }
}
