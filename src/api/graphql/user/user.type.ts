import { ObjectType, Field, registerEnumType } from 'type-graphql';

import { Role, Domain } from 'src/core';
import { NodeType } from '../common';
import { DomainType } from '../domain';

registerEnumType(Role, {
    name: 'UserRole',
});

@ObjectType()
export class UserType extends NodeType {
    @Field()
    email!: string;

    @Field(type => Role)
    role!: Role;

    @Field(type => DomainType)
    domain!: Domain;

    @Field(type => [DomainType])
    allowedDomains!: Domain[];

    @Field({ nullable: true })
    firstName?: string;

    @Field({ nullable: true })
    lastName?: string;
}
