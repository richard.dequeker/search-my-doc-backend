import { ArgsType, Field, ID } from 'type-graphql';

import { UserType } from '../user.type';

@ArgsType()
export class FindOneUserArgs {
    @Field(type => ID, { nullable: true })
    id?: string;

    @Field({ nullable: true })
    email?: string;
}
