import { Field, ArgsType } from 'type-graphql';

import { UserType } from '../user.type';

@ArgsType()
export class FindUsersArgs {
    @Field({ nullable: true })
    email?: string;
}
