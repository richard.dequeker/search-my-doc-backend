import { Resolver, Query, Args, Mutation, Arg } from 'type-graphql';

import { User } from 'src/core';
import { UserType } from './user.type';
import { UserService } from 'src/api/services';
import { FindUsersArgs, FindOneUserArgs } from './args';
import { CreateUserInput, UpdateUserInput, DeleteUserInput } from './inputs';

@Resolver(of => UserType)
export class UserResolver {
    constructor(private readonly userService: UserService) {}

    @Query(returns => [UserType], { description: 'Find many users' })
    async users(@Args() where?: FindUsersArgs): Promise<User[]> {
        return this.userService.find({ where });
    }

    @Query(returns => UserType, { description: 'Find one user' })
    async user(@Args() where?: FindOneUserArgs): Promise<User> {
        return await this.userService.findOne({ where });
    }

    @Mutation(returns => UserType, { description: 'Create one user' })
    async createUser(@Arg('user') input: CreateUserInput): Promise<User> {
        return await this.userService.create(input);
    }

    @Mutation(returns => UserType, { description: 'Update one user' })
    async updateUser(@Arg('user') user: UpdateUserInput) {
        return await this.userService.update(user);
    }

    @Mutation(returns => String, { description: 'Delete one user' })
    async deleteUser(@Arg('user') user: DeleteUserInput) {
        return await this.userService.delete(user.id);
    }
}
