import { Field, InputType, ID } from 'type-graphql';
import { IsEmail } from 'class-validator';

import { Role } from 'src/core';

@InputType()
export class CreateUserInput {
    @Field()
    @IsEmail()
    email!: string;

    @Field()
    password!: string;

    @Field(type => ID)
    domainId!: string;

    @Field(type => [ID])
    allowedDomainIds!: string[];

    @Field({ nullable: true })
    firstName?: string;

    @Field({ nullable: true })
    lastName?: string;

    @Field(type => Role)
    role!: Role;
}
