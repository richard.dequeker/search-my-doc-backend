import { Field, ID, InputType } from 'type-graphql';

import { Node } from 'src/core';

@InputType()
export class DeleteUserInput {
    @Field(type => ID)
    id!: string;
}
