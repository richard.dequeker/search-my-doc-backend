import { Field, ID, InputType } from 'type-graphql';

import { UserType } from '../user.type';
import { Role } from 'src/core';

@InputType()
export class UpdateUserInput {
    @Field(type => ID)
    id!: string;

    @Field({ nullable: true })
    firstName?: string;

    @Field({ nullable: true })
    lastName?: string;

    @Field({ nullable: true })
    password?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;

    @Field(type => [ID], { nullable: true })
    allowedDomainIds!: string[];

    @Field(type => Role, { nullable: true })
    role?: Role;
}
