import { ObjectType, Field } from 'type-graphql';

import { Domain } from 'src/core';
import { NodeType } from '../common';

@ObjectType()
export class DomainType extends NodeType implements Partial<Domain> {
    @Field()
    name!: string;

    @Field({ nullable: true })
    description?: string;
}
