import { ArgsType, Field, ID } from 'type-graphql';

@ArgsType()
export class FindOneDomainArgs {
    @Field(type => ID, { nullable: true })
    id?: string;

    @Field({ nullable: true })
    name?: string;

    @Field({ nullable: true })
    description?: string;
}
