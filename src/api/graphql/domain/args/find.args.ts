import { Field, ArgsType } from 'type-graphql';

@ArgsType()
export class FindDomainsArgs {
    @Field({ nullable: true })
    name?: string;

    @Field({ nullable: true })
    description?: string;
}
