import { Field, InputType } from 'type-graphql';

@InputType()
export class CreateDomainInput {
    @Field()
    name!: string;

    @Field({ nullable: true })
    description?: string;
}
