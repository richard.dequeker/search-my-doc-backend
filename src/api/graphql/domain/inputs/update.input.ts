import { Field, ID, InputType } from 'type-graphql';

@InputType()
export class UpdateDomainInput {
    @Field(type => ID)
    id!: string;

    @Field({ nullable: true })
    name?: string;

    @Field({ nullable: true })
    description?: string;
}
