import { Field, ID, InputType } from 'type-graphql';

@InputType()
export class DeleteDomainInput {
    @Field(type => ID)
    id!: string;
}
