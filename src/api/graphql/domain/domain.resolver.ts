import { Query, Resolver, Arg, Mutation, Args } from 'type-graphql';

import { Domain } from 'src/core';
import { DomainService } from 'src/api/services';
import { DomainType } from './domain.type';
import { FindDomainsArgs, FindOneDomainArgs } from './args';
import { CreateDomainInput, UpdateDomainInput, DeleteDomainInput } from './inputs';

@Resolver()
export class DomainResolver {
    constructor(private readonly domainService: DomainService) {}

    @Query(returns => DomainType, { description: 'Find one domain' })
    async domain(@Args() where: FindOneDomainArgs): Promise<Domain> {
        return await this.domainService.findOne({ where });
    }

    @Query(returns => [DomainType], { description: 'Find many domains' })
    async domains(@Args() where?: FindDomainsArgs): Promise<Domain[]> {
        return await this.domainService.find({ where });
    }

    @Mutation(returns => DomainType, { description: 'create one domain' })
    async createDomain(@Arg('domain') { name, description }: CreateDomainInput) {
        return await this.domainService.create({ name, description });
    }

    @Mutation(returns => DomainType, { description: 'Update one domain' })
    async updateDomain(@Arg('domain') domain: UpdateDomainInput) {
        return await this.domainService.update(domain.id, domain);
    }

    @Mutation(returns => String, { description: 'Delete one domain' })
    async deleteDomain(@Arg('domain') domain: DeleteDomainInput) {
        return await this.domainService.delete(domain.id);
    }
}
