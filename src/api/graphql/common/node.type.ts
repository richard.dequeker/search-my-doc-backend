import { ObjectType, Field, ID } from 'type-graphql';

import { Node } from 'src/core';

@ObjectType()
export class NodeType implements Node {
    @Field(type => ID)
    id!: string;

    @Field(type => String)
    createdAt!: Date;

    @Field(type => String)
    updatedAt!: Date;
}
