import { registerEnumType } from 'type-graphql';

import { ArchiveStatus } from 'src/core';

registerEnumType(ArchiveStatus, {
    name: 'ArchiveStatus',
});
