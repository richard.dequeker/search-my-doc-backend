import { ObjectType, Field, Int } from 'type-graphql';

import { NodeType } from '../common';
import { RuleArchiveType } from '../rule/rule-archive.type';
import { DomainType } from '../domain';
import { Domain } from 'src/core';

@ObjectType()
export class DocumentType extends NodeType {
    @Field(type => Int)
    version!: number;

    @Field(type => Int)
    revision!: number;

    @Field()
    title!: string;

    @Field(type => [RuleArchiveType])
    rules!: RuleArchiveType[];

    @Field({ nullable: true })
    content?: string;

    @Field(type => DomainType)
    domain!: DomainType;
}
