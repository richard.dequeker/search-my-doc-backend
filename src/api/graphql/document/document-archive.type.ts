import { ObjectType, Field } from 'type-graphql';

import { ArchiveStatus } from 'src/core';
import { DocumentType } from './document.type';
import '../common/archive-status.enum';

@ObjectType()
export class DocumentArchiveType extends DocumentType {
    @Field(type => ArchiveStatus)
    status!: ArchiveStatus;

    @Field(type => DocumentType, { nullable: true })
    draft!: DocumentType;
}
