import { ArgsType, Field, ID } from 'type-graphql';
import { FindOneDomainArgs } from '../../domain/args';
import { DomainType } from '../../domain';

@ArgsType()
export class FindOneDocumentArgs {
    @Field(type => ID, { nullable: true })
    id?: string;

    @Field({ nullable: true })
    title?: string;

    @Field({ nullable: true })
    description?: string;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;
}
