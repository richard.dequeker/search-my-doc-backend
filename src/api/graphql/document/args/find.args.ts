import { Field, ArgsType, ID } from 'type-graphql';

@ArgsType()
export class FindDocumentsArgs {
    @Field({ nullable: true })
    title?: string;

    @Field({ nullable: true })
    description?: string;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;
}
