import { Field, ArgsType, ID } from 'type-graphql';
import { ArchiveStatus } from 'src/core';

@ArgsType()
export class FindDocumentArchivesArgs {
    @Field({ nullable: true })
    title?: string;

    @Field({ nullable: true })
    description?: string;

    @Field(type => ArchiveStatus, { nullable: true })
    status?: ArchiveStatus;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;

    @Field(type => ID, { nullable: true })
    draftId?: string;
}
