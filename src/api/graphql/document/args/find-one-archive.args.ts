import { ArgsType, Field, ID } from 'type-graphql';
import { ArchiveStatus } from 'src/core';

@ArgsType()
export class FindOneDocumentArchiveArgs {
    @Field(type => ID, { nullable: true })
    id?: string;

    @Field({ nullable: true })
    title?: string;

    @Field(type => ArchiveStatus, { nullable: true })
    status?: ArchiveStatus;

    @Field({ nullable: true })
    description?: string;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;

    @Field(type => ID, { nullable: true })
    draftId?: string;
}
