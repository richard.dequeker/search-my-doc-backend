import { ObjectType, Field, Int } from 'type-graphql';

import { DocumentType } from './document.type';
import { PartialRuleType } from '../rule';

@ObjectType()
export class PartialDocumentType {
    @Field(type => Int)
    version!: number;

    @Field(type => Int)
    revision!: number;

    @Field()
    title!: string;

    @Field(type => [PartialRuleType])
    rules!: PartialRuleType[];

    @Field({ nullable: true })
    content?: string;
}
