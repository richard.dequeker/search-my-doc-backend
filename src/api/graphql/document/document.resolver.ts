import { Resolver, Mutation, Arg, Query, Args } from 'type-graphql';
import { FileUpload, GraphQLUpload } from 'graphql-upload';

import { DocumentService } from 'src/api/services';
import { DocumentType } from './document.type';
import {
    FindDocumentsArgs,
    FindOneDocumentArgs,
    FindDocumentArchivesArgs,
    FindOneDocumentArchiveArgs,
} from './args';
import { CreateDocumentInput, UpdateDocumentInput, DeleteDocumentInput } from './inputs';
import { PartialDocumentType } from './partial-document.type';
import { PublishDocumentInput } from './inputs/publish.input';
import { DocumentArchiveType } from './document-archive.type';

@Resolver(of => DocumentType)
export class DocumentResolver {
    constructor(private readonly documentService: DocumentService) {}

    @Query(returns => [DocumentType], { description: 'Find many documents' })
    async documents(@Args() where?: FindDocumentsArgs): Promise<Document[]> {
        if (where?.domainId) {
            const domainId = where.domainId;
            delete where.domainId;
            return this.documentService.find({ where: { ...where, domain: { id: domainId } } });
        }
        return await this.documentService.find({ where });
    }

    @Query(returns => DocumentType, { description: 'Find one document' })
    async document(@Args() where?: FindOneDocumentArgs): Promise<Document> {
        if (where?.domainId) {
            const domainId = where.domainId;
            delete where.domainId;
            return this.documentService.findOne({ where: { ...where, domain: { id: domainId } } });
        }
        return await this.documentService.findOne({ where });
    }

    @Query(returns => [DocumentArchiveType], { description: 'Find many documents archive' })
    async documentArchives(@Args() where?: FindDocumentArchivesArgs): Promise<Document[]> {
        let finalWhere: any = null;
        if (where?.domainId) {
            const domainId = where.domainId;
            finalWhere = { domain: { id: domainId } };
            delete where.domainId;
        }
        if (where?.draftId) {
            const draftId = where.draftId;
            finalWhere = { draft: { id: draftId }, ...finalWhere };
            delete where.draftId;
        }
        return await this.documentService.findArchives({ where: { ...where, ...finalWhere } });
    }

    @Query(returns => DocumentArchiveType, { description: 'Find one document archive' })
    async documentArchive(@Args() where?: FindOneDocumentArchiveArgs): Promise<Document> {
        let finalWhere: any = null;
        if (where?.domainId) {
            const domainId = where.domainId;
            finalWhere = { domain: { id: domainId } };
            delete where.domainId;
        }
        if (where?.draftId) {
            const draftId = where.draftId;
            finalWhere = { draft: { id: draftId }, ...finalWhere };
            delete where.draftId;
        }
        return await this.documentService.findOneArchive({
            where: { ...where, ...finalWhere },
        });
    }

    @Mutation(returns => DocumentArchiveType, { description: 'Publish a document' })
    async publishDocument(@Arg('document') document: PublishDocumentInput) {
        return await this.documentService.publish(document);
    }

    @Mutation(returns => DocumentArchiveType, { description: 'Create a new document' })
    async createDocument(@Arg('document') document: CreateDocumentInput) {
        return await this.documentService.create(document);
    }

    @Mutation(returns => DocumentArchiveType, { description: 'Update a document' })
    async updateDocument(@Arg('document') document: UpdateDocumentInput) {
        return await this.documentService.update(document);
    }
    @Mutation(returns => String, { description: 'Delete one document' })
    async deleteDocument(@Arg('document') document: DeleteDocumentInput) {
        return await this.documentService.delete(document.id);
    }

    @Mutation(returns => PartialDocumentType, {
        description: 'Upload document and return parsed results as partial document and rules',
    })
    async uploadDocument(@Arg('document', type => GraphQLUpload) file: FileUpload) {
        return await this.documentService.upload(file);
    }
}
