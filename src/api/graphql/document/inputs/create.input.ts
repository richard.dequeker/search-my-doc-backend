import { Field, InputType, ID } from 'type-graphql';

@InputType()
export class CreateDocumentInput {
    @Field()
    title!: string;

    @Field()
    version!: number;

    @Field()
    revision!: number;

    @Field(type => ID)
    domainId!: string;

    @Field(type => [ID])
    ruleIds!: string[];

    @Field({ nullable: true })
    description?: string;

    @Field({ nullable: true })
    content?: string;
}
