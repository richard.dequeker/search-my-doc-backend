import { Field, ID, InputType } from 'type-graphql';
import { DocumentType } from '../document.type';

@InputType()
export class UpdateDocumentInput implements Partial<DocumentType> {
    @Field(type => ID)
    id!: string;

    @Field({ nullable: true })
    title?: string;

    @Field({ nullable: true })
    description?: string;

    @Field({ nullable: true })
    content?: string;

    @Field(type => ID, { nullable: true })
    domainId?: string;

    @Field(type => [ID], { nullable: true })
    ruleIds?: string[];

    @Field({ nullable: true, defaultValue: false })
    major?: boolean;
}
