import { Field, ID, InputType } from 'type-graphql';

@InputType()
export class PublishDocumentInput {
    @Field(type => ID)
    id!: string;
}
