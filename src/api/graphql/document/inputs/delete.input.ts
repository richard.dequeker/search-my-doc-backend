import { Field, ID, InputType } from 'type-graphql';

import { Node } from 'src/core';

@InputType()
export class DeleteDocumentInput implements Node {
    @Field(type => ID)
    id!: string;
}
