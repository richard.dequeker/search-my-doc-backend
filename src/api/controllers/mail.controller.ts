import { Router, Request, Response, NextFunction } from 'express';
import Container from 'typedi';

import { EntityNotFoundError } from 'src/core';
import { clientConfig } from 'src/utils/config';
import { MailService } from '../services';
import { AuthService } from '../services';

export const MailController = Router();

MailController.get('/:uuid', async (req: Request, res: Response, next: NextFunction) => {
    const uuid = req.params.uuid;
    const {
        url,
        redirections: { resetPassword },
    } = clientConfig;
    try {
        const mailService = Container.get(MailService);
        const authService = Container.get(AuthService);
        const user = await mailService.getUserFromUUID(uuid);
        const token = authService.generateToken(user);
        return res.status(307).redirect(`${url}${resetPassword}?token=${token}`);
    } catch (error) {
        next(error);
    }
});
