import { AuthChecker } from 'type-graphql';

import { AuthContext } from './types';

export const authChecker: AuthChecker<AuthContext> = (
    { context: { user } },
    roles: string | string[]
) => {
    if (roles.length === 0) {
        return user !== undefined;
    }
    if (!user) {
        return false;
    }
    if (roles.includes(user.role)) {
        return true;
    }
    return false;
};
