import { Request } from 'express';
import { ContextFunction } from 'apollo-server-core';
import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';

import { User } from 'src/core';

import { AuthContext } from './types';
import { checkAuthorization } from './check-authorization';

export const appContext: ContextFunction<ExpressContext, object> = async ({
    req,
}: {
    req: Request;
}): Promise<AuthContext> => {
    const user: User | undefined = await checkAuthorization(req);
    return { user };
};
