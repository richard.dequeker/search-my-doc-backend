import { User } from 'src/core';

export interface AuthContext {
    user?: User;
}
