import { verify } from 'jsonwebtoken';
import { AuthenticationError } from 'apollo-server';
import { Request } from 'express';
import Container from 'typedi';

import { User } from 'src/core';
import { securityConfig } from 'src/utils/config';
import { UserService } from '../services';

export async function checkAuthorization(req: Request): Promise<User | undefined> {
    const { authorization } = req.headers;
    const [schema, token] = authorization ? authorization.split(' ') : '';
    if (schema === 'Bearer' && token) {
        try {
            const { id } = verify(token, securityConfig.jwt.secret) as Partial<User>;
            const userService = Container.get(UserService);
            return await userService.findOne({ where: { id } });
        } catch (err) {
            throw new AuthenticationError('You must log in');
        }
    }
}
