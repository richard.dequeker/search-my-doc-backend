import { MiddlewareFn } from 'type-graphql';

import { Logger } from 'src/utils/logger';

/**
 * Log Requests
 */
export const LogRequest: MiddlewareFn = async ({ info }, next) => {
    const start = Date.now();
    await next();
    const resolveTime = Date.now() - start;
    if (info.parentType.name === 'Query' || info.parentType.name === 'Mutation') {
        Logger.info(
            `${info.parentType.name} on ${info.fieldName} [\x1b[33m${resolveTime}\x1b[0m ms]`
        );
    }
};
