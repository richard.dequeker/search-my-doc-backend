import { GraphQLError } from 'graphql';

import { Logger } from 'src/utils/logger';
import { ServerError, UnauthorizedError } from 'src/core';

/**
 * Handle api errors
 */
export function formatError(error: any) {
    if (error instanceof GraphQLError) {
        Logger.error(error.message);
        return error;
    }
    if (process.env.NODE_ENV !== 'production') {
        console.error(error);
    }
    if (error.extensions.code === 'UNAUTHENTICATED') {
        console.log(error);
        throw new UnauthorizedError();
    }
    return new ServerError();
}
