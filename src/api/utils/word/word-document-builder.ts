import { WordDocument } from './word-document';
import { DocumentBuilder } from 'src/utils/document';

export class WordDocumentBuilder extends DocumentBuilder<WordDocument> {
      document: WordDocument = new WordDocument();
}
