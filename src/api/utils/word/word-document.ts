import { IDocument, IVersionInfo, IChapter } from 'src/utils/document';
import { Document, Rule } from 'src/core';

export class WordDocument implements IDocument {
    title: string = '';
    version: number = 1;
    revision: number = 0;
    versionInfos: IVersionInfo[] = [];
    chapters: IChapter[] = [];

    toDocument(): Partial<Document> {
        const [introductionChapter] = this.chapters.filter(chapter =>
            chapter.title.toLowerCase().includes('introduction')
        );
        let content = '';
        if (introductionChapter) {
            content = introductionChapter.paragraphs.toString();
        }
        return {
            title: this.title,
            version: this.version,
            revision: this.revision,
            content,
        };
    }

    toRules(): Partial<Rule>[] {
        const rules: Partial<Rule>[] = [];
        this.chapters
            .filter(chapter => !chapter.title.toLowerCase().includes('introduction'))
            .forEach(chapter => this.mapChapter(chapter, rules));
        return rules;
    }

    private mapChapter(chapter: IChapter, rules: Partial<Rule>[]) {
        const content = chapter.paragraphs.reduce((prev, curr) => prev + curr, '');
        if (chapter.subChapters.length) {
            chapter.subChapters.forEach(subChapter => this.mapChapter(subChapter, rules));
        }
        rules.push({
            title: chapter.title,
            version: this.version,
            revision: this.revision,
            content,
            image: chapter.image as any,
        });
    }
}
