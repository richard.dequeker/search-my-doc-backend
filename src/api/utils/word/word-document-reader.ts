import { convertToHtml } from 'mammoth';
import cheerio from 'cheerio';

import { DocumentReader } from 'src/utils/document';

export class WordDocumentReader extends DocumentReader {
    private documentRef!: CheerioStatic;

    getTitle() {
        return this.documentRef('p')
            .first()
            .text();
    }

    getVersion() {
        return this.getVersionAndRevision()[0];
    }

    getRevision() {
        return this.getVersionAndRevision()[1];
    }

    getVersionInfos() {
        const infos: any[] = [];
        this.documentRef('table')
            .first()
            .find('tr')
            .each((rowIndex: number, rowElement: CheerioElement) => {
                const info = this.getVersionInfoFromRow(rowElement);
                infos.push(info);
            });
        return infos;
    }

    getChapters() {
        const chapters: any[] = [];
        this.documentRef('h1').each((titleIndex: number, titleElement) => {
            const titleElementRef = this.documentRef(titleElement);
            const chapter = this.getChapterFromTitle(titleElementRef);
            chapters.push(chapter);
        });
        return chapters;
    }

    protected async load() {
        const { value } = await convertToHtml({ buffer: this.documentBuffer });
        this.documentRef = cheerio.load(value);
        this.emit(this.EventType.LOADED);
    }

    private getVersionAndRevision(): number[] {
        const versionAndRevision: RegExpMatchArray | null = this.documentRef('p')
            .first()
            .next()
            .text()
            .match(/\d/g);
        if (!versionAndRevision || versionAndRevision.length !== 2) {
            throw new Error('Invalid format');
        }
        return versionAndRevision.map(n => parseInt(n, 10));
    }

    private getVersionInfoFromRow(rowElement: CheerioElement) {
        const info = { author: '', date: '', action: '' };
        const rowElementRef = this.documentRef(rowElement);
        rowElementRef.find('td').each((columnIndex: number, columnElement: CheerioElement) => {
            const columnElementRef = this.documentRef(columnElement);
            switch (columnIndex) {
                case 0:
                    info.author = columnElementRef.text();
                    break;
                case 1:
                    info.action = columnElementRef.text();
                    break;
                case 2:
                    info.date = columnElementRef.text();
                    break;
                default:
                    throw new Error('Invalid format for version infos');
            }
        });
        return info;
    }

    private getChapterFromTitle(titleElementRef: Cheerio, level = 1) {
        const chapter: {
            title: string;
            paragraphs: string[];
            subChapters: any[];
            image?: string;
        } = {
            title: titleElementRef.text(),
            paragraphs: [],
            subChapters: [],
        };
        const chapterElements = this.documentRef(titleElementRef).nextUntil(`h${level}`);
        let shouldExtractMoreParagraph = true;

        chapterElements.each((elementIndex: number, element: CheerioElement) => {
            const elementRef = this.documentRef(element);
            switch (element.tagName) {
                case 'p':
                    if (shouldExtractMoreParagraph) {
                        if (elementRef.text().length) {
                            chapter.paragraphs.push(elementRef.text());
                        } else if (elementRef.children().first()[0].tagName === 'img') {
                            chapter.image = elementRef.children().first()[0].attribs.src;
                        }
                    }
                    break;
                case 'ul':
                    if (shouldExtractMoreParagraph) {
                        elementRef.children().each((listItemIndex: number, listItemElement) => {
                            const listItemElementRef = this.documentRef(listItemElement);
                            chapter.paragraphs.push(listItemElementRef.text());
                        });
                    }
                    break;
                case `h${level + 1}`:
                    const subChapter = this.getChapterFromTitle(elementRef, level + 1);
                    chapter.subChapters.push(subChapter);
                    shouldExtractMoreParagraph = false;
                    break;
                case `h${level - 1}`:
                    shouldExtractMoreParagraph = false;
                    break;
            }
        });
        return chapter;
    }
}
