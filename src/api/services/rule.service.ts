import { Service, Inject } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

import {
    RuleRepository,
    Rule,
    EntityExistError,
    EntityNotFoundError,
    RuleArchiveRepository,
    RuleArchive,
    ArchiveStatus,
} from 'src/core';
import { UpdateRuleInput, CreateRuleInput } from '../graphql/rule/inputs';
import { FindOneOptions, FindManyOptions } from 'typeorm';
import { DomainService } from './domain.service';

@Service()
export class RuleService {
    @InjectRepository(RuleRepository)
    protected readonly repository!: RuleRepository;

    @InjectRepository(RuleArchiveRepository)
    protected readonly archiveRepository!: RuleArchiveRepository;

    @Inject()
    private readonly domainService!: DomainService;

    /**
     * Find many draft
     */
    async find(options: FindManyOptions<Rule>) {
        return await this.repository.find({ ...options, relations: ['domain'] });
    }
    /**
     * Find one draft
     */
    async findOne(options: FindOneOptions<Rule>) {
        const rule = await this.repository.findOne({ ...options, relations: ['domain'] });
        if (!rule) {
            throw new EntityNotFoundError();
        }
        return rule;
    }
    /**
     * Find many archives
     */
    async findArchive(options: FindManyOptions<RuleArchive>) {
        return await this.archiveRepository.find({
            ...options,
            relations: ['domain', 'draft'],
        });
    }
    /**
     *  Find one archive
     */
    async findOneArchive(options: FindOneOptions<RuleArchive>) {
        const ruleArchive = await this.archiveRepository.findOne({
            ...options,
            relations: ['domain', 'draft'],
        });
        if (!ruleArchive) {
            throw new EntityNotFoundError();
        }
        return ruleArchive;
    }
    /**
     * create new draft and archive the return archive
     */
    async create({ domainId, ...data }: CreateRuleInput): Promise<RuleArchive> {
        const ruleExist = await this.repository.findOne({ where: { title: data.title } });
        if (ruleExist) {
            throw new EntityExistError(`Rule with title ${data.title} already exist`);
        }
        const domain = await this.domainService.findOne({ where: { id: domainId } });
        const rule = await this.repository.create({ domain, ...data });
        await this.repository.save(rule);
        return await this.createArchive(rule.id);
    }
    /**
     * update draft and create archive then return archive
     */
    async update({ id, domainId, major, ...data }: UpdateRuleInput) {
        const rule = await this.findOne({ where: { id } });
        if (domainId) {
            const domain = await this.domainService.findOne({ where: { id: domainId } });
            rule.domain = domain;
        }
        if (major) {
            rule.version++;
        } else {
            rule.revision++;
        }
        await this.repository.save({ ...rule, ...data });
        return await this.createArchive(id);
    }
    /**
     * publish archive, unpublish other
     */
    async publish({ id }: { id: string }): Promise<RuleArchive> {
        const ruleArchive = await this.findOneArchive({ where: { id } });

        const lastPublished = await this.archiveRepository.findOne({
            where: { status: ArchiveStatus.PUBLISHED, draft: ruleArchive.draft },
        });

        if (lastPublished) {
            lastPublished.status = ArchiveStatus.ARCHIVED;
            await this.archiveRepository.save(lastPublished);
        }
        ruleArchive.status = ArchiveStatus.PUBLISHED;
        const archive = await this.archiveRepository.save(ruleArchive);
        return await this.findOneArchive({ where: { id: archive.id } });
    }
    /**
     * @deprecated
     * ! Not working
     * TODO cascading delete
     */
    async delete(id: string) {
        await this.findOne({ where: { id } });
        await this.repository.delete(id);
        const archiveIds = await (
            await this.archiveRepository.find({ where: { draft: { id } } })
        ).map(archive => archive.id);
        await this.archiveRepository.delete(archiveIds);
        return id;
    }
    /**
     * Create archive from draft id
     */
    private async createArchive(ruleId: string) {
        const draft = await this.findOne({ where: { id: ruleId } });
        const archive = this.archiveRepository.create({ draft });
        delete draft.id;
        const archiveEntity = await this.archiveRepository.save({ ...draft, ...archive });
        return await this.findOneArchive({ where: { id: archiveEntity.id } });
    }
}
