import { Service } from 'typedi';
import { User, EmailUUIDRepository, EntityNotFoundError } from 'src/core';
import { InjectRepository } from 'typeorm-typedi-extensions';
import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';

import { Logger } from 'src/utils/logger';
import { smtpConfig, apiConfig } from 'src/utils/config';

@Service()
export class MailService {
    @InjectRepository(EmailUUIDRepository)
    private readonly repository!: EmailUUIDRepository;

    private readonly sender = `"Find My Doc" <${smtpConfig.email}>`;
    private readonly mailSubject = 'Reset Email 🔒';

    async sendMail(user: User) {
        const uuidExist = await this.repository.findOne({ where: { user } });
        if (uuidExist) {
            await this.repository.delete(uuidExist.id);
        }
        const mailUUID = await this.repository.create({ user });
        const { id } = await this.repository.save(mailUUID);
        const transporter = this.createTransporter();
        const info = await transporter.sendMail({
            from: this.sender,
            to: user.email,
            subject: this.mailSubject,
            html: this.createMailTemplate(id),
        });
        Logger.info(`Mail sended to ${user.email}: link: ${nodemailer.getTestMessageUrl(info)} \n`);
    }

    async getUserFromUUID(uuid: string): Promise<User> {
        const emailUUID = await this.repository.findOne(uuid, { relations: ['user'] });
        if (!emailUUID) {
            throw new EntityNotFoundError();
        }
        await this.repository.delete(emailUUID);
        return emailUUID.user;
    }

    private createTransporter(): Mail {
        return nodemailer.createTransport({
            host: smtpConfig.host,
            port: smtpConfig.port,
            secure: false,
            auth: {
                user: smtpConfig.email,
                pass: smtpConfig.password,
            },
        });
    }

    private createMailTemplate(id: string) {
        return `
        <div>
            Cliquez <a href="${this.createLink(id)}">ici</a>
        </div>`;
    }

    private createLink(id: string) {
        return `${apiConfig.url}${apiConfig.paths.resetPassword}/${id}`;
    }
}
