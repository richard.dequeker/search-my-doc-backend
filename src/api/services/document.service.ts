import { Service, Inject } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';
import { FileUpload } from 'graphql-upload';

import {
    DocumentRepository,
    Document,
    DocumentArchiveRepository,
    EntityExistError,
    EntityNotFoundError,
    RuleArchive,
    DocumentArchive,
    ArchiveStatus,
} from 'src/core';
import { WordDocumentService } from './word-document.service';
import { WordDocument } from '../utils/word';
import { RuleService } from './rule.service';
import { FindOneOptions, FindManyOptions, In } from 'typeorm';
import { UpdateDocumentInput, CreateDocumentInput } from '../graphql/document/inputs';
import { DomainService } from './domain.service';

@Service()
export class DocumentService {
    @InjectRepository(DocumentRepository)
    protected readonly repository!: DocumentRepository;

    @InjectRepository(DocumentArchiveRepository)
    protected readonly archiveRepository!: DocumentArchiveRepository;

    @Inject()
    private readonly wordDocumentService!: WordDocumentService;

    @Inject()
    private readonly ruleService!: RuleService;

    @Inject()
    private readonly domainService!: DomainService;

    /**
     * Find many draft
     */
    async find(options: FindManyOptions<Document>) {
        return await this.repository.find({ ...options, relations: ['rules', 'domain'] });
    }
    /**
     * Find one draft
     */
    async findOne(options: FindOneOptions<Document>) {
        const user = await this.repository.findOne({
            ...options,
            relations: ['rules', 'domain'],
        });
        if (!user) {
            throw new EntityNotFoundError();
        }
        return user;
    }
    /**
     * Find many archives
     */
    async findArchives(options: FindManyOptions<DocumentArchive>) {
        return await this.archiveRepository.find({
            ...options,
            relations: ['rules', 'domain', 'draft'],
        });
    }

    /**
     *  Find one archive
     */
    async findOneArchive(options: FindOneOptions<DocumentArchive>) {
        const user = await this.archiveRepository.findOne({
            ...options,
            relations: ['rules', 'domain', 'draft'],
        });
        if (!user) {
            throw new EntityNotFoundError();
        }
        return user;
    }
    /**
     * create new draft and archive the return archive
     */
    async create({ ruleIds, domainId, ...data }: CreateDocumentInput) {
        const documentExist = await this.repository.findOne({ where: { title: data.title } });
        if (documentExist) {
            throw new EntityExistError(`Document with title ${data.title} already exist`);
        }
        const domain = await this.domainService.findOne({ where: { id: domainId } });
        const rules: RuleArchive[] = ruleIds.length
            ? await this.ruleService.findArchive({
                  where: { id: In(ruleIds) },
              })
            : [];
        const document = await this.repository.create({ ...data, domain, rules });
        await this.repository.save(document);
        return await this.createArchive(document.id);
    }
    /**
     * update draft and create archive then return archive
     */
    async update({ id, ruleIds, domainId, major, ...data }: UpdateDocumentInput) {
        const document = await this.repository.findOne(id);
        if (!document) {
            throw new EntityNotFoundError();
        }
        if (ruleIds) {
            const rules: RuleArchive[] = ruleIds.length
                ? await this.ruleService.findArchive({
                      where: { id: In(ruleIds) },
                  })
                : [];
            document.rules = rules;
        }
        if (domainId) {
            const domain = await this.domainService.findOne({ where: { id: domainId } });
            document.domain = domain;
        }
        if (major) {
            document.version++;
        } else {
            document.revision++;
        }
        await this.repository.save({ ...document, ...data });
        return await this.createArchive(document.id);
    }

    /**
     * publish archive, unpublish other
     */
    async publish({ id }: { id: string }): Promise<DocumentArchive> {
        const documentArchive = await this.findOneArchive({ where: { id } });
        const lastPublished = await this.archiveRepository.findOne({
            where: { status: ArchiveStatus.PUBLISHED, draft: documentArchive.draft },
        });
        if (lastPublished) {
            lastPublished.status = ArchiveStatus.ARCHIVED;
            await this.archiveRepository.save(lastPublished);
        }
        documentArchive.status = ArchiveStatus.PUBLISHED;
        const archive = await this.archiveRepository.save(documentArchive);
        return await this.findOneArchive({ where: { id: archive.id } });
    }

    /**
     * Upload docx
     */
    async upload(upload: FileUpload) {
        const wordDocument: WordDocument = await this.wordDocumentService.upload(upload);
        const document = wordDocument.toDocument();
        const rules = wordDocument.toRules();
        return { ...document, rules };
    }

    /**
     * @deprecated
     * ! Not working
     * TODO cascading delete
     */
    async delete(id: string) {
        await this.findOne({ where: { id } });
        await this.repository.delete(id);
        const archiveIds = await (
            await this.archiveRepository.find({ where: { draft: { id } } })
        ).map(archive => archive.id);
        await this.archiveRepository.delete(archiveIds);
        return id;
    }

    /**
     * Create archive from draft id
     */
    async createArchive(documentId: string) {
        const draft = await this.findOne({ where: { id: documentId } });
        const archive = this.archiveRepository.create({ draft });
        delete draft.id;
        const archiveEntity = await this.archiveRepository.save({ ...draft, ...archive });
        return await this.findOneArchive({ where: { id: archiveEntity.id } });
    }
}
