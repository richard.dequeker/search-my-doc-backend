import { Service } from 'typedi';
import { sign } from 'jsonwebtoken';

import { User, UnauthorizedError } from 'src/core';
import { UserService } from './user.service';
import { AuthType } from '../graphql';
import { AuthContext } from '../context/types';
import { securityConfig } from 'src/utils/config';

@Service()
export class AuthService {
    constructor(private readonly userService: UserService) {}
    /**
     * Log in and return user and jsonwebtoken
     */
    async signIn(email: string, password: string): Promise<AuthType> {
        const user = await this.userService.findOne({ where: { email } });
        if (!user.isPasswordValid(password)) {
            throw new UnauthorizedError();
        }
        const token = this.generateToken(user);
        return { user, token };
    }
    /**
     * Check if authenticated user in context then return it with new jsonwebtoken
     */
    async authenticate({ user }: AuthContext) {
        if (!user) {
            throw new UnauthorizedError();
        }
        const token = this.generateToken(user);
        return { user, token };
    }
    /**
     * Generate a new jsonwebtoken
     */
    generateToken(user: User): string {
        return sign(
            {
                id: user.id,
                role: user.role,
            },
            securityConfig.jwt.secret,
            {
                subject: user.email,
            }
        );
    }
}
