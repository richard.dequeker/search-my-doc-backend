import { Service, Inject } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

import { UserRepository, User, EntityNotFoundError, EntityExistError } from 'src/core';
import { DomainService } from './domain.service';
import { CreateUserInput, UpdateUserInput } from '../graphql/user/inputs';
import { FindOneOptions, FindManyOptions, In } from 'typeorm';
import { EntityService } from './types';
import { MailService } from './mail.service';
@Service()
export class UserService implements EntityService<User> {
    @InjectRepository(UserRepository)
    readonly repository!: UserRepository;

    @Inject()
    private readonly domainService!: DomainService;

    @Inject()
    private readonly mailService!: MailService;

    async findOne(options: FindOneOptions<User>) {
        const user = await this.repository.findOne({
            ...options,
            relations: ['domain', 'allowedDomains'],
        });
        if (!user) {
            throw new EntityNotFoundError();
        }
        return user;
    }

    async find(options: FindManyOptions<User>) {
        return await this.repository.find({ ...options, relations: ['domain', 'allowedDomains'] });
    }

    async create({ email, password, domainId, allowedDomainIds, ...data }: CreateUserInput) {
        const userExist = await this.repository.findOne({ email });
        if (userExist) {
            throw new EntityExistError();
        }
        allowedDomainIds = Array.isArray(allowedDomainIds) ? allowedDomainIds : [];
        const domain = await this.domainService.findOne({ where: { id: domainId } });
        const allowedDomains = allowedDomainIds.length
            ? await this.domainService.find({
                  where: { id: In(allowedDomainIds) },
              })
            : [];
        const user = await this.repository.create({ email, domain, allowedDomains, ...data });
        user.password = password;
        const { id } = await this.repository.save(user);
        // send email for password change
        await this.mailService.sendMail(user);
        return await this.findOne({ where: { id } });
    }

    async update({ id, password, domainId, allowedDomainIds, ...data }: UpdateUserInput) {
        const user = await this.findOne({ where: { id } });
        if (password) {
            user.password = password;
        }
        if (domainId) {
            const domain = await this.domainService.findOne({ where: { id: domainId } });
            user.domain = domain;
        }
        if (allowedDomainIds) {
            const allowedDomains = allowedDomainIds.length
                ? await this.domainService.find({
                      where: { id: In(allowedDomainIds) },
                  })
                : [];
            user.allowedDomains = allowedDomains;
        }
        await this.repository.save({ ...user, ...data });
        return await this.findOne({ where: { id } });
    }

    async delete(id: string) {
        await this.findOne({ where: { id } });
        await this.repository.delete(id);
        return id;
    }
}
