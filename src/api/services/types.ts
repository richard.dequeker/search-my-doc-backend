import { Repository } from 'typeorm';

export interface Read<T> {
    find(...args: any[]): Promise<T[]>;
    findOne(...args: any[]): Promise<T>;
}

export interface Write<T> {
    create(...args: any[]): Promise<T>;
    update(data: Partial<T> & { id: string }): Promise<T>;
    delete(id: string): Promise<string>;
}

export interface EntityService<T> extends Read<T>, Write<T> {
    readonly repository: Repository<T>;
}

export interface ArchiveService<T, A> extends EntityService<T> {
    readonly archiveRepository: Repository<A>;
}
