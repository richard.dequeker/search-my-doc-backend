import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';
import {
    UserRepository,
    DomainRepository,
    Role,
    ServerError,
    User,
    Domain,
    EntityNotFoundError,
} from 'src/core';

import { Random } from 'src/utils/random';

@Service()
export class SeederService {
    @InjectRepository() userRepository!: UserRepository;
    @InjectRepository() domainRepository!: DomainRepository;

    async seedAdmin({ name, description }: Partial<Domain>, email: string, password: string) {
        try {
            // Create admin domain
            let domain = await this.domainRepository.findOne({
                where: { name },
            });
            if (!domain) {
                domain = this.domainRepository.create({
                    name,
                    description,
                });
                domain = await this.domainRepository.save(domain);
            }
            // Create admin
            let admin = await this.userRepository.findOne({
                where: { email },
            });
            // if admin does no exist
            if (!admin) {
                admin = this.userRepository.create({
                    email,
                    role: Role.ADMIN,
                    domain,
                    allowedDomains: [domain],
                });
            }
            admin.password = password;
            return await this.userRepository.save(admin);
        } catch (err) {
            throw new ServerError(err.message);
        }
    }

    async seedRandomDomains(count: number = 10) {
        // Create random domain data
        const domainsData = Random.createManyRandom<Partial<Domain>>(count, {
            name: 'title',
        });
        // Create domain
        const domains = this.domainRepository.create(domainsData);
        return await this.domainRepository.save(domains);
    }

    async seedRandomUsers(count: number = 100, password?: string) {
        // get all domains
        const domains = await this.domainRepository.find();
        if (!domains.length) {
            throw new EntityNotFoundError('You must create domains before');
        }
        // Create random user data
        const usersData = Random.createManyRandom<Partial<User>>(count, {
            email: 'email',
            firstName: 'first_name',
            lastName: 'last_name',
        });
        // Create and save users from data
        const users = usersData.map(data => {
            let allowedDomains: Domain[] = [];
            while (!allowedDomains.length) {
                const length = Random.number(1, 5);
                allowedDomains = Random.from(domains, length); // get randoms allowed domains
            }
            const domain = Random.oneFrom(allowedDomains); // Get one random domain
            const user = this.userRepository.create({
                allowedDomains,
                domain,
                ...data,
            });
            user.password = password ? password : Random.casual('password');
            return user;
        });
        return await this.userRepository.save(users);
    }
}
