import { Service } from 'typedi';

import { FileUpload } from 'graphql-upload';

@Service()
export class UploadService {
    createBufferFromUpload(upload: FileUpload): Promise<Buffer> {
        const stream = upload.createReadStream(); // create stream from upload
        const chuncks: Buffer[] = [];
        return new Promise((resolve, reject) => {
            stream.on('error', reject);
            stream.on('data', (chunck: Buffer) => chuncks.push(chunck)); // get chuncks
            stream.on('end', () => resolve(Buffer.concat(chuncks))); // return buffer from chuncks
        });
    }
}
