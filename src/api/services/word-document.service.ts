import { Service, Inject } from 'typedi';
import { FileUpload } from 'graphql-upload';

import { WordDocumentReader, WordDocumentBuilder, WordDocument } from '../utils/word';
import { UploadService } from './upload.service';

@Service()
export class WordDocumentService {
    @Inject()
    private readonly uploadService!: UploadService;

    async upload(upload: FileUpload): Promise<WordDocument> {
        const buffer = await this.uploadService.createBufferFromUpload(upload); // get buffer from upload
        const documentReader = new WordDocumentReader({ buffer }); // create reader
        const documentBuilder = new WordDocumentBuilder(documentReader); // create builder with reader
        await documentBuilder.buildDocument(); // build
        return documentBuilder.getDocument(); // get document data
    }
}
