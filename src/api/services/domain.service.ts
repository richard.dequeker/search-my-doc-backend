import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';

import { DomainRepository, Domain, EntityExistError, EntityNotFoundError } from 'src/core';
import { FindManyOptions, FindOneOptions, FindConditions } from 'typeorm';

@Service()
export class DomainService {
    @InjectRepository(DomainRepository)
    readonly repository!: DomainRepository;

    async findOne(options: FindOneOptions<Domain>) {
        const domain = await this.repository.findOne(options);
        if (!domain) {
            throw new EntityNotFoundError();
        }
        return domain;
    }

    async find(options?: FindManyOptions<Domain>) {
        return await this.repository.find(options);
    }

    async create({ name, description }: Pick<Domain, 'name' | 'description'>) {
        const domainExist = await this.repository.findOne({ name });
        if (domainExist) {
            throw new EntityExistError();
        }
        const domain = await this.repository.create({ name, description });
        return await this.repository.save(domain);
    }

    async update(id: string, data: Partial<Domain>) {
        delete data.id;
        if (!id) {
            throw new EntityNotFoundError();
        }
        return await this.repository.save({ id, ...data });
    }

    async delete(id: string) {
        const domain = await this.findOne({ where: { id } });
        await this.repository.delete(domain);
        return id;
    }
}
