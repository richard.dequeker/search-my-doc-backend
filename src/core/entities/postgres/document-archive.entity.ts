import { Entity, Column, ManyToOne } from 'typeorm';

import { Archive, ArchiveStatus } from '../../types';
import { Document } from './document.entity';

@Entity('document_archive')
export class DocumentArchive extends Document implements Archive<Document> {
    @Column({ type: 'enum', enum: ArchiveStatus, default: ArchiveStatus.ARCHIVED })
    status!: ArchiveStatus;

    @ManyToOne(type => Document)
    draft!: Document;
}
