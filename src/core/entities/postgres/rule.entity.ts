import { Entity, Column, ManyToOne } from 'typeorm';

import { Versionable } from '../../types';
import { Domain } from './domain.entity';
import { BaseEntity } from './base-entity';

@Entity('rule')
export class Rule extends BaseEntity implements Versionable {
    @Column()
    version!: number;

    @Column()
    revision!: number;

    @Column()
    title!: string;

    @Column()
    content!: string;

    @Column({ nullable: true })
    image?: string;

    @ManyToOne(type => Domain)
    domain!: Domain;
}
