import { Entity, OneToOne, JoinColumn } from 'typeorm';

import { BaseEntity } from './base-entity';
import { User } from './user.entity';

@Entity('email-uuid')
export class EmailUUID extends BaseEntity {
    @OneToOne(type => User)
    @JoinColumn()
    user!: User;
}
