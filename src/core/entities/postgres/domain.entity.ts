import { Column, Entity } from 'typeorm';

import { BaseEntity } from './base-entity';

@Entity('domain')
export class Domain extends BaseEntity {
    @Column({ unique: true })
    name!: string;

    @Column({ nullable: true })
    description?: string;
}
