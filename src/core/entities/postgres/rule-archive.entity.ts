import { Entity, Column, ManyToOne } from 'typeorm';

import { Archive, ArchiveStatus } from '../../types';
import { Rule } from './rule.entity';

@Entity('rule_archive')
export class RuleArchive extends Rule implements Archive<Rule> {
    @Column({ type: 'enum', enum: ArchiveStatus, default: ArchiveStatus.ARCHIVED })
    status!: ArchiveStatus;

    @ManyToOne(type => Rule)
    draft!: Rule;
}
