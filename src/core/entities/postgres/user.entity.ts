import { Entity, Column, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import crypto from 'crypto';

import { Domain } from './domain.entity';
import { BaseEntity } from './base-entity';

export enum Role {
    UNKNOWN = 'UNKNOWN',
    USER = 'USER',
    MAINTAINER = 'MAINTAINER',
    ADMIN = 'ADMIN',
}
@Entity('user')
export class User extends BaseEntity {
    @Column({ unique: true })
    email!: string;

    @Column({ type: 'enum', enum: Role, default: Role.USER })
    role!: Role;

    @Column()
    hash!: string;

    @Column({ nullable: true })
    firstName?: string;

    @Column({ nullable: true })
    lastName?: string;

    @ManyToOne(type => Domain)
    domain!: Domain;

    @ManyToMany(type => Domain)
    @JoinTable()
    allowedDomains!: Domain[];

    @Column()
    private salt!: string;

    constructor(email: string, password: string) {
        super();
        this.email = email;
        if (password) {
            this.password = password;
        }
    }

    set password(password: string) {
        this.salt = crypto.randomBytes(16).toString('hex');
        this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    }

    isPasswordValid(password: string): boolean {
        const hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
        return this.hash === hash;
    }
}
