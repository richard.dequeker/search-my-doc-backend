import { Column, Entity, ManyToMany, JoinTable, ManyToOne } from 'typeorm';

import { Versionable } from '../../types';
import { BaseEntity } from './base-entity';
import { Domain } from './domain.entity';
import { RuleArchive } from './rule-archive.entity';

@Entity('document')
export class Document extends BaseEntity implements Versionable {
    @Column()
    version!: number;

    @Column()
    revision!: number;

    @Column()
    title!: string;

    @Column({ nullable: true })
    content?: string;

    @ManyToOne(type => Domain)
    domain!: Domain;

    @ManyToMany(type => RuleArchive)
    @JoinTable()
    rules!: RuleArchive[];
}
