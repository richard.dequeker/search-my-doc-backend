import { PrimaryGeneratedColumn, BeforeInsert, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import uuidv4 from 'uuid';

import { Node } from '../../types';

export abstract class BaseEntity implements Node {
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @CreateDateColumn()
    createdAt!: Date;

    @UpdateDateColumn()
    updatedAt!: Date;

    @BeforeInsert()
    private generateID() {
        this.id = uuidv4();
    }
}
