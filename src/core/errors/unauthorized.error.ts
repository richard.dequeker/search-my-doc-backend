import { ApolloServerError } from './apollo-server.error';
import { ErrorCode } from './types';

export class UnauthorizedError extends ApolloServerError {
    constructor(message = 'Unauthorized') {
        super(message, ErrorCode.UNAUTHORIZED);
    }
}
