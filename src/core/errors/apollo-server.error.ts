import { ErrorCode } from './types';

export abstract class ApolloServerError extends Error {
    readonly code: ErrorCode = ErrorCode.INTERNAL_SERVER_ERROR;
    constructor(message = 'Server error', code?: ErrorCode) {
        super(message);
        if (code) {
            this.code = code;
        }
    }
}
