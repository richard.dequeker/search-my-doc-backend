import { ApolloServerError } from './apollo-server.error';
import { ErrorCode } from './types';

export class EntityNotFoundError extends ApolloServerError {
    constructor(message = 'Entity not found') {
        super(message, ErrorCode.NOT_FOUND);
    }
}
