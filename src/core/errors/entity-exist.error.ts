import { ErrorCode } from './types';
import { ApolloServerError } from './apollo-server.error';

export class EntityExistError extends ApolloServerError {
    constructor(message = 'Entity already exist') {
        super(message, ErrorCode.CONFLICT);
    }
}
