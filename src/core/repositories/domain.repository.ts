import { EntityRepository, Repository } from 'typeorm';
import { Domain } from '../entities';

@EntityRepository(Domain)
export class DomainRepository extends Repository<Domain> {}
