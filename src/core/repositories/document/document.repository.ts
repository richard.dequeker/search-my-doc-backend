import { EntityRepository, Repository } from 'typeorm';

import { Document } from '../../entities';

@EntityRepository(Document)
export class DocumentRepository extends Repository<Document> {}
