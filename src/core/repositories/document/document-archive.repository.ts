import { EntityRepository, Repository } from 'typeorm';

import { DocumentArchive } from '../../entities';

@EntityRepository(DocumentArchive)
export class DocumentArchiveRepository extends Repository<DocumentArchive> {}
