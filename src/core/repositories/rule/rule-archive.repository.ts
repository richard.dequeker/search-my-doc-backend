import { EntityRepository, Repository } from 'typeorm';

import { RuleArchive } from '../../entities';

@EntityRepository(RuleArchive)
export class RuleArchiveRepository extends Repository<RuleArchive> {}
