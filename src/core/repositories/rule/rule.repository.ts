import { EntityRepository, Repository } from 'typeorm';

import { Rule } from '../../entities';

@EntityRepository(Rule)
export class RuleRepository extends Repository<Rule> {}
