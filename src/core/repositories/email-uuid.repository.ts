import { Repository, EntityRepository } from 'typeorm';
import { EmailUUID } from '../entities';

@EntityRepository(EmailUUID)
export class EmailUUIDRepository extends Repository<EmailUUID> {}
