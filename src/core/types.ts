export interface Node {
    id: string;
}

export enum ArchiveStatus {
    ARCHIVED = 'ARCHIVED',
    PUBLISHED = 'PUBLISHED',
}

export interface Archive<T> {
    status: ArchiveStatus;
    draft: T;
}

export interface Versionable {
    version: number;
    revision: number;
}
