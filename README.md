# SEARCH MY DOC

## Install & Run

-   Install PostgreSQL v11.5

-   Clone git repository

```sh
git clone https://gitlab.com/richard.dequeker/search-my-doc-backend.git
```

-   Install node js packages

```sh
cd search-my-doc-backend && npm i
```

-   Create a file named 'default.json' in the config folder and copy this config
    You need to set your own values
    you can create a fake smtp user here: https://ethereal.email

```json
{
    // Database options
    "connection": {
        "name": "default",
        "type": "postgres",
        "host": "localhost",
        "port": 5432,
        "database": "find-my-doc",
        "syncronize": true,
        "username": "postgres",
        "password": "root"
    },
    "api": {
        "url": "http://localhost:4000",
        "port": 4000,
        "paths": {
            "resetPassword": "/rest/auth/reset_password"
        }
    },
    "client": {
        "url": "http://localhost:4200",
        "redirections": {
            "resetPassword": "/reset_password"
        }
    },
    "security": {
        "cors": {
            "origin": "*"
        },
        "jwt": {
            "secret": "my_secret_key"
        },
        "administration": {
            "email": "admin@admin.com",
            "password": "Admin1234!",
            "domain": {
                "name": "Administration",
                "description": "Administration"
            }
        }
    },
    "smtp": {
        "host": "smtp.ethereal.email",
        "port": 587,
        "email": "da.lton@ethereal.email",
        "password": "my_email_secret"
    }
}
```

-   Run app

```
npm start
```

## TECHNOS

### Front end Angular / React / Vue

-   GraphQL
-   Apollo client

### Back end Node Typescript / .NET CORE

-   Type ORM (Node)
-   Apollo server (Node)
-   Entity framework (c#)
-   GraphQL (Node/C#)

## ARCHITECTURE

### Front end

#### Single Page Application

Une application web monopage (en anglais single-page application ou SPA) est une application web accessible via une page web unique. Le but est d'éviter le chargement d'une nouvelle page à chaque action demandée, et de fluidifier ainsi l'expérience utilisateur. Deux méthodes existent pour ce faire : l'ensemble des éléments de l'application est chargé (contenu, images, CSS et JavaScript) dans un unique fichier HTML, soit les ressources nécessaires sont récupérées et affichées dynamiquement en fonction des actions de l'utilisateur.

cf: https://fr.wikipedia.org/wiki/Application_web_monopage

### Back end

#### Data Mapper Pattern

Un mappeur de données est une couche d'accès aux données qui effectue un transfert de données bidirectionnel entre un magasin de données persistant (souvent une base de données relationnelle ) et une représentation de données en mémoire (la couche de domaine). L'objectif du modèle est de conserver la représentation en mémoire et le magasin de données persistantes indépendants l'un de l'autre et du mappeur de données lui-même.
Les mappeurs génériques gèrent de nombreux types d'entités de domaine différents, les mappeurs dédiés en gèrent un ou plusieurs.

cf: https://en.wikipedia.org/wiki/Data_mapper_pattern

### Back / Front

#### GraphQL

La particularité de GraphQL est que la structure de la réponse du serveur est fixée par le client (conformément aux limites de l'API). Par exemple, c'est lui qui décide des champs qu'il souhaite pour chaque Objet, et dans quel ordre il veut les recevoir.

cf: https://fr.wikipedia.org/wiki/GraphQL

### Documentations / Tutorials

> For all GraphQL client and server documentations, see the graphql chapter

#### Typescript

-   https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html
-   https://www.typescriptlang.org/docs/handbook/utility-types.html
-   https://palantir.github.io/tslint/rules/

#### Angular

-   https://angular.io/tutorial
-   https://angular.io/guide/styleguide
-   https://material.angular.io/

#### React

-   https://fr.reactjs.org/tutorial/tutorial.html
-   https://fr.reactjs.org/docs/hooks-intro.html
-   https://arc.js.org/

#### Vue

-   https://vuejs.org/

#### Node.js

-   https://nodejs.org/dist/latest-v12.x/docs/api/
-   https://nodejs.org/api/stream.html
-   https://expressjs.com/fr/4x/api.html

#### .NET

-   https://docs.microsoft.com/fr-fr/ef/core/
-   https://docs.microsoft.com/fr-fr/dotnet/csharp/programming-guide/concepts/linq/introduction-to-linq-queries
-   https://docs.microsoft.com/fr-fr/dotnet/csharp/programming-guide/inside-a-program/coding-conventions

#### GraphQL

-   https://graphql.org/code/
-   https://graphql.org/code/#javascript
-   https://graphql.org/code/#java
-   https://graphql.org/code/#c-net
-   https://www.howtographql.com/
-   https://www.prisma.io/tutorials/
-   https://www.apollographql.com/docs/apollo-server/
-   https://www.apollographql.com/docs/react/
-   https://www.apollographql.com/docs/angular/
-   https://github.com/vuejs/vue-apollo
