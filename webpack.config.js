const NodemonWebpackPlugin = require('nodemon-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const { TsConfigPathsPlugin } = require('awesome-typescript-loader');

const path = require('path');

module.exports = {
    name: 'server',
    target: 'node',
    mode: 'development',
    watch: true,
    entry: {
        server: './src/index.ts',
    },
    output: {
        path: path.resolve(__dirname, 'build', 'server'),
        filename: '[name].bundle.js',
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: ['awesome-typescript-loader'],
            },
        ],
    },
    plugins: [new NodemonWebpackPlugin()],
    externals: [nodeExternals()],
    resolve: {
        extensions: ['.ts', '.js'],
        plugins: [
            new TsConfigPathsPlugin({
                configFileName: './tsconfig.json',
            }),
        ],
    },
};
